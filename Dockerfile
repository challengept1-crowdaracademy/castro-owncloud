FROM ubuntu:20.04
RUN apt-get update && apt-get upgrade -y && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN  apt-get update \
  && apt-get install --no-install-recommends -y wget \
  && rm -rf /var/lib/apt/lists/*
#A partir de aquí me guío por la documentación de Owncloud para realizar la imagen
#Preparando Ubuntu 20.04
#Instalar PHP 7.4
RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections \
    && apt-get install --no-install-recommends -y -q
RUN apt-get update && apt-get install --no-install-recommends php -y \
    && apt-get install --no-install-recommends php-mysql php-mbstring php-intl php-redis php-imagick \
    php-igbinary php-gmp php-bcmath php-curl php-gd php-zip php-imap \
    php-ldap php-bz2 php-ssh2 php-phpseclib php-fpm php-cgi -y\
    && apt-get install --no-install-recommends php-dev libsmbclient-dev php-pear -y\
    && apt-get install --no-install-recommends smbclient -y && apt-get install --no-install-recommends redis-server -y\
    && apt-get install --no-install-recommends unzip -y && apt-get install --no-install-recommends openssl -y\
    && pear version && mkdir -p /tmp/pear/cache \
    && pear upgrade --force --alldeps http://pear.php.net/get/PEAR-1.10.12 \
    && pear clear-cache && pear update-channels \
    && pear upgrade --force && pear upgrade-all && pear version \
    && rmdir /tmp/pear/cache --ignore-fail-on-non-empty && apt-get clean && rm -rf /var/lib/apt/lists/*
#Instalar Apache2
RUN apt-get update && apt-get install --no-install-recommends -y apache2 && apt-get clean && rm -rf /var/lib/apt/lists/*
ENV APACHE_RUN_USER	www-data
ENV APACHE_RUN_GROUP 	www-data
ENV APACHE_LOG_DIR 	/var/log/apache2
ENV APACHE_PID_FILE 	/var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR	/var/run/apache2
ENV APACHE_LOCK_DIR	/var/lock/apache2
VOLUME var/www/html/
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR
EXPOSE 80
#Elegir version PHP (por las dudas)
RUN update-alternatives --set php /usr/bin/php7.4 \
    && update-alternatives --set phar /usr/bin/phar7.4 \
    && update-alternatives --set phar.phar /usr/bin/phar.phar7.4 \
    && update-alternatives --set phpize /usr/bin/phpize7.4 \
    && update-alternatives --set php-config /usr/bin/php-config7.4
#Librería libsmbclient-php
RUN pecl channel-update pecl.php.net \
    && pecl install smbclient
RUN echo extension=smbclient.so > /etc/php/7.4/mods-available/smbclient.ini \
    && phpenmod smbclient && service apache2 restart
#Descargar Owncloud
RUN wget --progress=dot:giga https://download.owncloud.org/community/owncloud-complete-20210326.tar.bz2 \
    && wget --progress=dot:giga https://download.owncloud.org/community/owncloud-complete-20210326.tar.bz2.sha256 \
    && sha256sum -c owncloud-complete-20210326.tar.bz2.sha256 < owncloud-complete-20210326.tar.bz2
EXPOSE 8080
#Instalar Owncloud (separo descarga de instalación para aprovechar caches generadas)
#Intento aprovechar las cache de cada paso porque voy corrigiendo este Dockerfile en mi PC
#Por eso hay tantos RUN y pido ignorar el error DL3059
RUN tar -xjf owncloud-complete-20210326.tar.bz2 \
    && cp -r owncloud /var/www \
    && service apache2 restart \
#Cambio usuario segun recomendaciones de Linter
ENTRYPOINT ["/usr/sbin/apache2"]
CMD ["-D", "FOREGROUND"]
#Configuración de Apache pendiente
#Configuraciones adicionales de Apache pendiente